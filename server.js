//Install express server
const express = require('express');
const cors = require('cors');
const app = express();


app.use(cors());
 
// Serve only the static files form the dist directory
// Replace the '/dist/<to_your_project_name>'
app.use(express.static(__dirname + '/dist/poc2-frontend'));

app.get('/*', function(req,res) {
  // Replace the '/dist/<to_your_project_name>/index.html'
  
  res.header("Access-Control-Allow-Origin", "*");
  res.sendFile(__dirname + '/dist/poc2-frontend/index.html');
});
// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080, () => {
  console.log('SERVER RUNNING...')
});