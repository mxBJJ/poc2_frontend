import { animate, transition } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  open: Boolean = false;

  constructor(private router : Router) { }

  lineForm = new FormGroup({
    linha: new FormControl('')
  });

  ngOnInit(): void {
  }

  isOpen(){
    this.open = !this.open;
    // console.log(this.open)
    return this.open;
  }

  onSubmit(){
    this.router.navigate(['/linha/search'], { queryParams: { linha: this.lineForm.value.linha }});
  }

}
