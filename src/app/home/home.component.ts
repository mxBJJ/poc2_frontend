import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { DataPoaService } from '../data-poa.service';
import { Line } from '../model/Line';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  tipo: String;
  displayedColumns: string[] = ['id', 'codigo', 'nome'];
  busLines : Line[] = [];
  ELEMENT_DATA: Line[] = [];
  dataSource = new MatTableDataSource<Line>(this.ELEMENT_DATA);
  
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataPoaService : DataPoaService, private route : ActivatedRoute, private router: Router) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

   }
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.tipo = params.tipo;
    })
    this.getBusLines()
  }

  getBusLines(){
    if(this.tipo != 'l'){
      this.tipo = 'o'
    }
    this.dataPoaService.listLines(this.tipo).subscribe(data => {   
      this.busLines = data
      this.ELEMENT_DATA = this.busLines;
      this.dataSource = new MatTableDataSource<Line>(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
    })
  }

  getItem(item: Line){
    console.log(item)
  }
}
