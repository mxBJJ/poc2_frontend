import { ILatLng } from './../directions-map.directive';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { __param } from 'tslib';


export class AuxPoint {
  lat: String
  lng: String
}

@Component({
  selector: 'app-map-route',
  templateUrl: './map-route.component.html',
  styleUrls: ['./map-route.component.css']
})
export class MapRouteComponent implements OnInit {

  displayDirections = true;
  zoom = 17;

  waypoints: ILatLng[] = [];
  destination: ILatLng;
  origin: ILatLng;

  points: AuxPoint[] = [];

  constructor(private route: ActivatedRoute) { }


  ngOnInit(): void {

    this.route.queryParams.subscribe(param => {

      this.points = JSON.parse(param.pontos)

      for (let item of this.points) {
        let wp: ILatLng = {
          latitude: Number(item.lat),
          longitude: Number(item.lng)
        }

        this.waypoints.push(wp)
      }

      console.log(this.waypoints)
      
      this.destination = {
        latitude: this.waypoints[(this.waypoints.length -1)].latitude,
        longitude: this.waypoints[(this.waypoints.length -1)].longitude
      }

      this.origin = {
        latitude: this.waypoints[0].latitude,
        longitude: this.waypoints[0].longitude
      }

      console.log(this.origin)
      console.log(this.destination)
    })
  }

}
