import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataPoaService } from '../data-poa.service';
import { Line } from '../model/Line';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  busLines: Line[] = [];
  minibusLines: Line[] = [];
  dataSource: Line[] = [];
  linha: String;
  showProgressBar: Boolean = true
  notFound: Boolean = false

  displayedColumns: string[] = ['id', 'nome', 'codigo'];

  constructor(private dataPoaService: DataPoaService, private route: ActivatedRoute, private router: Router) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.notFound = false
  }

  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      this.linha = params.linha
    })

    this.dataPoaService.listLines("o").subscribe(data => {
      this.busLines = data
      this.dataSource = this.busLines

      this.dataPoaService.listLines("l").subscribe(data => {
        this.minibusLines = data

        for (let item of this.minibusLines) {
          this.dataSource.push(item);
        }


        this.showProgressBar = false

        this.dataSource = this.busLines.filter(l => l.nome.includes(this.linha.toUpperCase()))

        if (this.dataSource.length == 0) {
          this.notFound = true
        }
        console.log(this.notFound)
      });
    });
  }
}
