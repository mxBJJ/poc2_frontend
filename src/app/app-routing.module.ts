import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { HomeComponent } from './home/home.component';
import { ItineraryComponent } from './itinerary/itinerary.component';
import { MapRouteComponent } from './map-route/map-route.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  { path: 'itinerario', component: ItineraryComponent },
  { path: 'itinerario/rota', component: MapRouteComponent },
  { path: 'linha/search', component: DetailComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
