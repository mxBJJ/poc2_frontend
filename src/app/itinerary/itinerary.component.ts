import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { DataPoaService } from '../data-poa.service';

export class Point {
  lat: Number
  lng: Number
}

@Component({
  selector: 'app-itinerary',
  templateUrl: './itinerary.component.html',
  styleUrls: ['./itinerary.component.css']
})

export class ItineraryComponent implements OnInit {

  linha: String;
  itinerario: any;
  points: Point[] = [];

  displayedColumns: string[] = ['id', 'mapa'];
  ELEMENT_DATA: Point[] = [];
  dataSource = new MatTableDataSource<Point>(this.ELEMENT_DATA);
  nome: String;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataPoaService: DataPoaService, private route: ActivatedRoute, 
    private router : Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.linha = params.linha
      this.nome = params.nome

    });

    this.itinerario = this.dataPoaService.getItineraryByLine(this.linha).subscribe(
      data => {
        this.itinerario = data
        // console.log(this.itinerario)
        var count = 0;
        for (var key in data) {
          if(key != 'codigo' && key != 'nome' && key != 'idlinha'){
              let point = new Point()
              point = data[key]
              this.points.push(point)
          }
        }
        this.ELEMENT_DATA = this.points;
        this.dataSource = new MatTableDataSource<Point>(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      }
    );
  }


  goToMap(){
    console.log('GO TO MAP OK')
    this.router.navigate(['/itinerario/rota'], { queryParams: { pontos : JSON.stringify(this.points)}});
  }

}
