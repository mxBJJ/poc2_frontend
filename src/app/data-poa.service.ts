import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Line } from './model/Line';

@Injectable({
  providedIn: 'root'
})
export class DataPoaService {

  constructor(public http : HttpClient) { }

  listLines(tipo : String) : Observable<Line[]>{
    return this.http.get<Line[]>(`https://api-transportepoa.herokuapp.com/linhas?t=${tipo}`)
  }

  getItineraryByLine(linha: String) : Observable<any>{
    return this.http.get(`https://api-transportepoa.herokuapp.com/itinerario?p=${linha}`)
  }

}
